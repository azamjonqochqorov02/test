<?php

use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }
    return redirect()->route('admin.home');
});

Auth::routes(['register' => true]);


Route::any('handle/payme/', function (\Illuminate\Http\Client\Request $request){
    switch ($request->method) {
        case 'CheckPerformTransaction':
            $response = [
                'allow' => true,
            ];
            return response($response);
            break;

        case 'CheckTransaction':

            $transaction = \App\Models\Transaction::find($request->params['id']);
            if (!$transaction){
                return response('Transaction not found',-31003);
            }else{

                return response([
                    'create_time' => $transaction->created_at,
                    'transaction' => $transaction->id,
                    'state' => 1,
                ], 200);

            }

        case 'CreateTransaction':

            $transaction =  \App\Models\Transaction::find($request->params['id']);
            if ($transaction) {
                if ($transaction->state != \App\Models\Transaction::STATE_CREATED) {
                    $this->response(
                        'Transaction found, but is not active.'
                    );
                }
            }

            $transaction = Transaction::create([
                'amount' => $request->amount,
                'currency_code' => 860,
                'state' => 1,
                'updated_time' => \Carbon\Carbon::now(),
                'comment' => 'success',
            ]);

            return response([
                'create_time' =>$transaction->updated_time,
                'transaction' => $transaction->id,
                'state' => $transaction->state,
                'receivers' => [
                    'amount'=>$transaction->amount,
                ],
            ]);

        case 'PerformTransaction':
            $transaction =  \App\Models\Transaction::find($request->params['id']);

            // if transaction not found, send error
            if (!$transaction) {
                return response('Transaction not found.');
            }

            switch ($transaction->state) {
                case Transaction::STATE_CREATED:
                        $perform_time = \Carbon\Carbon::now();
                        $transaction->state = Transaction::STATE_COMPLETED;
                        $transaction->updated_time = $perform_time;

                        $transaction->update();

                        return response([
                            'transaction' =>$transaction->id,
                            'perform_time' => $perform_time,
                            'state' =>  $transaction->state,
                        ], 200);

                case Transaction::STATE_COMPLETED: // handle complete transaction

                    return response([
                        'transaction' => $transaction->id,
                        'perform_time' => \Carbon\Carbon::now(),
                        'state' => $transaction->state,
                    ]);
            }

        case 'CancelTransaction':

            $transaction = \App\Models\Transaction::find($request->params['id']);

            // if transaction not found, send error
            if (!$transaction) {
                $this->response->error('Transaction not found.');
            }

            switch ($transaction->state) {
                // if already cancelled, just send it
                case Transaction::STATE_CANCELLED:

                    return response([
                        'transaction' => $transaction->id,
                        'cancel_time' => \Carbon\Carbon::now(),
                        'state' => $transaction->state,
                    ]);

                case Transaction::STATE_CREATED:
                    // cancel transaction with given reason
                    $cancel_time = \Carbon\Carbon::now();

                    $detail = $transaction->detail;

                    $transaction->update([
                        'updated_time' => $cancel_time,
                    ]);

                    return response([
                        'transaction' =>$transaction->id,
                        'cancel_time' => $cancel_time,
                        'state' => $transaction->state,
                    ]);

                case Transaction::STATE_COMPLETED:
                    if (true) {

                        return response([
                            'transaction' =>$transaction->id,
                            'cancel_time' => \Carbon\Carbon::now(),
                            'state' => $transaction->state,
                        ]);
                    } else {
                        return response('Could not cancel transaction. Order is delivered/Service is completed.');
                    }
            }


        case 'ChangePassword':

            return response(['success' => true]);

        default:
            return response(
                'Method not found.',
                $request->method
            );
    }
});




Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
    // Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
