<div class="row">
    @foreach($products as $product)
        <div class="col-lg-4 col-sm-4">
            <div class="box_main">
                <h4 class="shirt_text">{{$product->name}}</h4>
                <p class="price_text">Price  <span style="color: #262626;">{{number_format($product->price)}}</span></p>
                <div class="tshirt_img"><img src="{{asset('assets/images/'.$product->image)}}"></div>
                <div class="btn_main">
                    <div class="buy_bt"><a href="javascript:void(0);" wire:click="buy('{{$product->price}}')">Buy Now</a href="javascript:void"></div>
                    <div class="seemore_bt"><a href="#">See More</a></div>
                </div>
            </div>
        </div>
    @endforeach

</div>
