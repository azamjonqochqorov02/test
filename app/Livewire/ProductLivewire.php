<?php

namespace App\Livewire;

use App\Models\Product;
use App\Models\Transaction;
use Carbon\Carbon;
use Livewire\Component;

class ProductLivewire extends Component
{
    public  $products;

    public function buy($price)
    {
        Transaction::create([
            'amount'=>$price,
            'currency_code'=>860,
            'state'=>1,
            'updated_time'=>Carbon::now(),
            'comment'=>'success',
        ]);

        $user_id = auth()->user()->id;
        $url = "https://developer.help.paycom.uz/payme/" . $user_id . "/" . $price . "?redirect_url=".route('admin.home');
        return  redirect($url);
    }

    public function render()
    {
        $this->products = Product::all();
        return view('livewire.product-livewire');
    }
}
