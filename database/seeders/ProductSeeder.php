<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'name'           => 'Man T -shirt',
                'price'          => '150000',
                'image'          => 'tshirt-img.png'
            ],
            [
                'name'           => 'Man T -shirt',
                'price'          => '250000',
                'image'          => 'dress-shirt-img.png'
            ],
            [
                'name'           => 'Woman Scart',
                'price'          => '250000',
                'image'          => 'women-clothes-img.png'
            ],
            [
                'name'           => 'Computers',
                'price'          => '3250000',
                'image'          => 'computer-img.png'
            ],
            [
                'name'           => 'Mobile',
                'price'          => '1250000',
                'image'          => 'mobile-img.png'
            ],
            [
                'name'           => 'Laptop',
                'price'          => '4250000',
                'image'          => 'laptop-img.png'
            ],


        ];

        Product::insert($products);
    }
}
